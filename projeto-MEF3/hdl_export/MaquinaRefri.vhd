library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


entity MaquinaRefri is
  port(
  --input ports below
    entrada      : in  std_logic_vector(3 downto 0);
    -- 0001 = 10
    -- 0010 = 25
    -- 0100 = 50
    -- 1000 = 100
    botao      : in  std_logic;
    clr        : in  std_logic;
    clk        : in  std_logic;
    
  --output ports below
    latinha    : out std_logic;
    retorno    : out integer
    );
end MaquinaRefri;

--------------------------------------------------------------------------------

architecture type_architecture of MaquinaRefri is

	
	signal estado: std_logic_vector(1 downto 0);
	-- 00 = coletando
	-- 01 = devolver
	-- 10 = correto
	-- 11 = liberar

begin

	process (clk, clr)

		variable valor : integer := 0;
		variable moeda : integer := 0;

	begin
		case entrada is
			when "0001" => -- 10
				moeda := 10;

			when "0010" => -- 25
				moeda := 25;

			when "0100" => -- 50
				moeda := 50;

			when "1000" => -- 100
				moeda := 100;

			when others =>
				moeda := 10; --caso padrão escolhido arbitráriamente
		end case;
		
		if clr = '1' then
			valor   := 0;
			estado  <= "00";
			latinha <= '0';
			retorno <= 0;
			moeda   := 0;
			
		elsif (clk'event and '1' = clk) then
			if botao = '1' then
				valor := 0;
				
				case estado is
					when "00" => --coletando
						retorno <= valor;
						estado  <= "01"; --devolver
						
					when "10" => --correto
						latinha <= '1';
						estado  <= "11"; --liberar

					when others =>
						valor := valor; -- nada muda
				end case;

			elsif (valor + moeda) < 100 then
				estado  <= "00"; -- coletando
				valor   := valor + moeda;
				latinha <= '0';
				retorno <= 0;
					
			elsif (valor + moeda) = 100 then
				estado  <= "10"; -- correto
				valor   := valor + moeda;
				latinha <= '0';
				retorno <= 0;

			elsif (valor + moeda) > 100 then
				estado  <= "01"; -- devolver
				retorno <= valor + moeda;
				valor   := 0;
			end if;
		end if;
	end process;
end type_architecture;
